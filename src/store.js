import {createStore, applyMiddleware} from "redux";

import reducer from './reducers'


const logAll = (store) => (next) => (action) => {
	console.log(action.type);
	return next(action)
}

const store = createStore(reducer,applyMiddleware(logAll));

export default store;