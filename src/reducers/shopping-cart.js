const updateCartItems = (cartItems, item, indx) => {

	if(item.count === 0){
		return [
			...cartItems.slice(0, indx),
			...cartItems.slice(indx+1),
		]
	}

	if(indx === -1){
		return [
			...cartItems,
			item
		]
	}

	return [
		...cartItems.slice(0, indx),
		item,
		...cartItems.slice(indx+1),
	]
};

const updateCartItem = (book, item = {}, qty) => {
	const {
		id = book.id,
		count = 0,
		title = book.title,
		total = 0
	} = item;

	return {
		id,
		title,
		count:count+qty,
		total:total+qty*book.price
	}

}

const updateOrder = (state, bookId, qty) => {

	const {bookList : {books}, shoppingCart: {cartItems}} = state;
	const book = books.find((book) => book.id === bookId);
	const itemIndex = cartItems.findIndex((book) => book.id === bookId);
	const item = cartItems[itemIndex];


	const newItem = updateCartItem(book, item, qty);

	return {
		orderTotal:0,
		cartItems: updateCartItems(cartItems, newItem, itemIndex)
	}

}

const updateShoppingCart = (state, action) => {

	if(state === undefined){
		return {
			cartItems: [],
			orderTotal:0
		}
	}


	switch (action.type) {
		case 'BOOK_ADDED_TO_CART':
			return updateOrder(state, action.payload, 1);
		case 'BOOK_REMOVED_FROM_CART':
			return updateOrder(state, action.payload, -1);

		case 'ALL_BOOKS_REMOVED_FROM_CART':
			const item = state.shoppingCart.cartItems.find((item) => item.id === action.payload);
			return updateOrder(state, action.payload, -item.count);
		default:
			return state.shoppingCart;
	}
}


export default updateShoppingCart