import React from 'react'
import {connect} from "react-redux";

import {
	bookAddedToCart,
	bookRemovedFromCart,
	allBookRemovedFromCart } from '../../actions';

const ShoppingCartTable = ({items, total, onIncrease, onDecrease, onDelete }) => {

	const renderRow = (item, indx) => {
		const {id, title, count, total} = item;
		return <tr key={id}>
			<td>{indx}</td>
			<td>{title}</td>
			<td>{count}</td>
			<td>{total} $</td>
			<td>
				<button
					onClick={() => onDelete(id)}
					className="btn btn-danger">
					<i className="fa fa-trash-o"/>
				</button>
				<button
					onClick={() => onIncrease(id)}
					className="btn btn-success">
					<i className="fa fa-plus-circle"/>
				</button>
				<button
					onClick={() => onDecrease(id)}
					className="btn btn-warning">
					<i className="fa fa-minus-circle"/>
				</button>
			</td>
		</tr>
	};

	return(
		<div className="shopping-cart-table">
			<h2>You'r order</h2>
			<table className="table">
				<thead>
				<tr>
					<th>#</th>
					<th>item</th>
					<th>count</th>
					<th>price</th>
					<th>action</th>
				</tr>
				</thead>
				<tbody>
				{items.map((item, ind) => renderRow(item, ind))}

				</tbody>
			</table>
			<div className="total">
				Total: {total}
			</div>
		</div>
	)
}

const mapStateToProps = ({shoppingCart}) => {
	return{
		items: shoppingCart.cartItems,
		total: shoppingCart.orderTotal
	}
};

const mapDispatchToProps = {
		onIncrease: bookAddedToCart,
		onDecrease: bookRemovedFromCart,
		onDelete: allBookRemovedFromCart
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ShoppingCartTable)