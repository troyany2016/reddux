import React, { Fragment } from 'react';

const BookListItem = ({book, onAddedToCart}) => {
	const { title, author, price, coverImage} = book;


	return (
		<Fragment>
			<img src={coverImage} alt="" className="coverImage"/>
			<span>{title}</span><br/>
			<span><strong>{author}</strong></span>
			<span>{price} $</span><br/>
			<button
				onClick={onAddedToCart}
				className="btn btn-primary btn-sm">
				Buy
			</button>
		</Fragment>
	)

};

export default BookListItem