import React from 'react'
import { Link } from 'react-router-dom'

const ShopHeader = ({numItems, total}) => {

		return (
			<header className="shop-header row">
				<Link to="/">
					<span className="logo text-dark">ReStore</span>
				</Link>
				<Link to="/cart">
					<i className="cart-icon fa fa-shopping-cart" />
					{numItems} items (${total})
				</Link>
			</header>
		);
}

export default ShopHeader