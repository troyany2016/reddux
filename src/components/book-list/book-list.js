import React, { Component } from 'react';
import BookListItem from '../book-list-item'
import { connect } from 'react-redux'
import { withBookStoreService } from '../hoc'
import { booksLoaded, booksRequest, booksError, bookAddedToCart } from "../../actions";
import { compose } from '../../utils'
import Spinner from '../spinner'
import ErrorIndicator from "../error-indicator";

const BookList =({books, onAddedToCart}) => {
	return (
		<div>
			<ul className="list-group">
				{books.map((book) => {
					return (
						<li className="list-group-item" key={book.id}>
							<BookListItem
								book={book}
								onAddedToCart={() => onAddedToCart(book.id)}
							/>
							</li>
					)
				})}
			</ul>
		</div>
	);
}


class BookListContainer extends Component{

	componentDidMount() {

		this.props.fetchBooks();
	}

	render() {
		const {books, loading, error, onAddedToCart} = this.props;
		if(loading){
			return <Spinner/>
		}
		if(error){
			return <ErrorIndicator/>
		}
		return <BookList books={books} onAddedToCart={onAddedToCart}/>
	}
}

const mapStateToProps = ({bookList}) => {
	return {
		books: bookList.books,
		loading: bookList.loading,
		error: bookList.error
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {

	const { bookstoreService } = ownProps;

	return {
		fetchBooks: () => {
			dispatch(booksRequest());
			bookstoreService.getBooks()
			.then((data) => dispatch(booksLoaded(data)))
			.catch((error) => dispatch(booksError(error)))

		},
		onAddedToCart: (id) => {
			dispatch(bookAddedToCart(id))
		}
	}

}

export default compose(
	withBookStoreService(),
	connect(mapStateToProps, mapDispatchToProps)
)(BookListContainer)