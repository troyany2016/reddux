class bookstoreServices {

	data = [
		{
			id:1,
			title: "Budda's little finger",
			author: 'Viktor Pelevin',
			price:32,
			coverImage:'https://st.kp.yandex.net/images/film_iphone/iphone360_609882.jpg'
		},
		{
			id:2,
			title: "Journey to Ixtlan",
			author: 'Karlos Kastaneda',
			price: 22,
			coverImage: 'https://images-na.ssl-images-amazon.com/images/I/51MOTv7b9iL._SX314_BO1,204,203,200_.jpg'
		}
	];

	getBooks() {

		return new Promise((resolve, reject) => {
			setTimeout(() => {
				if(Math.random() > 0.75){
					reject(new Error('Something bad hapened'))

				}else{
					resolve(this.data)
				}

			}, 700)
		})
	}
}

export default bookstoreServices